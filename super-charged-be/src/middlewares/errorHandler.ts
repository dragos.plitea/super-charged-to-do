import { Request, Response, NextFunction } from 'express';
import * as statusCodes from '@utils/statusCodes';

export const errorHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  console.error(err.stack);
  res.status(statusCodes.INTERNAL_SERVER_ERROR).json({ message: err.message });
};
