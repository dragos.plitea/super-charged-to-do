import fs from 'fs/promises';
import path from 'path';
import { Task } from './task.interfaces';

// Construct the file path to the tasks.json file
const filePath = path.join(__dirname, '../database/tasks.json');

// Function to read the tasks from the JSON file
const readFile = async (): Promise<Task[]> => {
  // Read the file content as a string
  // Parse the string content to JSON
  // Return the parsed JSON (array of tasks)
};

// Function to write the tasks to the JSON file
const writeFile = async (taskList: Task[]) => {
  // Convert the task list to a JSON string
  // Write the JSON string to the file
};

// Function to get all tasks
export const getAllTasks = async (): Promise<Task[]> => {
  // Call the readFile function to get all tasks
  // Return the list of tasks
};

// Function to create a new task
export const createTask = async (task: Task): Promise<Task> => {
  // Call the readFile function to get the current list of tasks
  // Create a new task object with an id and the provided task details
  // Add the new task to the list of tasks
  // Call the writeFile function to save the updated task list
  // Return the new task
};

// Function to update an existing task
export const updateTask = async (
  id: number,
  updatedTask: Task,
): Promise<Task | null> => {
  // Call the readFile function to get the current list of tasks
  // Find the task by id in the list of tasks
  // If the task is not found, return null
  // Update the found task with the provided updated task details
  // Call the writeFile function to save the updated task list
  // Return the updated task
};

// Function to delete an existing task
export const deleteTask = async (id: number) => {
  // Call the readFile function to get the current list of tasks
  // Find the task by id in the list of tasks
  // If the task is not found, return null
  // Remove the found task from the list of tasks
  // Call the writeFile function to save the updated task list
  // Return the deleted task
};
