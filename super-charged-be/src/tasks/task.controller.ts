import { Request, Response } from 'express';
import * as tasksService from './task.service';
import * as statusCodes from '@utils/statusCodes';

// Function to retrieve all tasks
export const getTasks = async (req: Request, res: Response) => {
  try {
    // Call the service function to get all tasks
    // Send back the retrieved tasks as JSON response
  } catch (error) {
    // If an error occurs, send back an internal server error response
  }
};

// Function to create a new task
export const createTask = async (req: Request, res: Response) => {
  try {
    // Extract title, description, and status from request body
    // If any of the required fields are missing, send a bad request response
    // Call the service function to create a new task with provided details
    // Send back the newly created task as JSON response
  } catch (error) {
    // If an error occurs, send back an internal server error response
  }
};

// Function to update an existing task
export const updateTask = async (req: Request, res: Response) => {
  try {
    // Extract task id, title, description, and status from request parameters and body
    // If any of the required fields are missing, send a bad request response
    // Call the service function to update the task with provided details
    // If the task is not found, send a not found response
    // Send back the updated task as JSON response
  } catch (error) {
    // If an error occurs, send back an internal server error response
  }
};

// Function to delete an existing task
export const deleteTask = async (req: Request, res: Response) => {
  try {
    // Extract task id from request parameters
    // Call the service function to delete the task
    // If the task is not found, send a not found response
    // Send back a success message indicating the task is deleted
  } catch (error) {
    // If an error occurs, send back an internal server error response
  }
};
