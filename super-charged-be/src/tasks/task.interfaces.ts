import { TaskStatus } from "./task.enums";

export interface Task {
  id?: number;
  title: string;
  description: string;
  status: TaskStatus;
}
